package types

import (
	"github.com/tendermint/go-amino"

	ntypes "gitlab.com/mayachain/binance-sdk/common/types"
	"gitlab.com/mayachain/binance-sdk/types/tx"
)

func NewCodec() *amino.Codec {
	cdc := amino.NewCodec()

	ntypes.RegisterWire(cdc)
	tx.RegisterCodec(cdc)
	return cdc
}
